import React from 'react';
import { Grid, Card, CardContent, Typography } from '@mui/material';

const CardGrid = () => {
  return (
    <Grid container spacing={2}>
      {Array.from({ length: 30 }, (_, index) => (
        <Grid item xs={4} key={index}>
          <Card>
            <CardContent>
              <Typography variant="h5" component="div">
                Card {index + 1}
              </Typography>
              <Typography color="text.secondary">
                This is a sample card content.
              </Typography>
            </CardContent>
          </Card>
        </Grid>
      ))}
    </Grid>
  );
};

export default CardGrid;
