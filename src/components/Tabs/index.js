import * as React from 'react';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import Box from '@mui/material/Box';

export default function TabList(mode) {
  const [value, setValue] = React.useState('one');

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <Box sx={{ width: '100%' }}>
      <Tabs
        value={value}
        onChange={handleChange}
        textColor={mode==0 ? "black": "white"}
        indicatorColor="secondary"
        aria-label="secondary tabs example"
      >
        <Tab 
          value="one" 
          label="기본흠" 
          sx={{color: mode === 0? 'black':'white'}} 
        />
        <Tab 
        value="two" 
        label="수2"
        sx={{color: mode === 0? 'black':'white'}}  />

        <Tab 
        value="three" 
        label="공간"
        sx={{color: mode === 0? 'black':'white'}}  />
      </Tabs>

    </Box>
  );
}
