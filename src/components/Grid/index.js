import * as React from 'react';
import { useState } from 'react';
import Grid from '@mui/material/Grid';
import { styled, createTheme, ThemeProvider } from '@mui/material/styles';
import Paper from '@mui/material/Paper';
import AddIcon from '@mui/icons-material/Add';

export default function Maingrid(mode) {

    const [matrix, setMatrix] = useState([
        [1, 0, 1, 1, 0, 1],
        [0, 0, 0, 0, 0, 0],
        [1, 0, 1, 1, 0, 1],
        [0, 0, 0, 0, 0, 0],
        [1, 0, 1, 1, 0, 1],
    ]);

    const customTheme = createTheme({
        palette: {
            primary: {
                main: '#1976d2',
                contrastText: 'white',
            },
        },
    });

    const ViewCard = styled(Paper)(({ border }) => ({
        width: 120,
        height: 120,
        textAlign: 'center',
        border: border === "true" ? "1px solid " : "none",
    }));

    const EditCard = styled(Paper)(({ theme, border }) => ({
        width: 120,
        height: 120,
        backgroundColor: '#1A2027',
        textAlign: 'center',
        color: theme.palette.primary.contrastText,
        border: border === "true" ? "1px solid " : "none",
    }));

    
  const handleDragStart = (e, row, col) => {
    e.dataTransfer.setData('row', row);
    e.dataTransfer.setData('col', col);
  };

  const handleDrop = (e, targetRow, targetCol) => {
    const draggedRow = parseInt(e.dataTransfer.getData('row'));
    const draggedCol = parseInt(e.dataTransfer.getData('col'));

    const newMatrix = [...matrix];
    newMatrix[targetRow][targetCol] = 1;
    newMatrix[draggedRow][draggedCol] = 0;
    setMatrix(newMatrix);
  };

    const renderView = () => {
        return matrix.map((row, ind) => {
            return row.map((col, index) => {
                if (col == 0) {
                    return (<Grid item xs={2} key={JSON.stringify({row: ind, col: index})}>
                        <ViewCard></ViewCard>
                    </Grid>)
                }
                else {
                    return (<Grid item xs={2} key={index}>
                        <ViewCard border="true">xs=3</ViewCard>
                    </Grid>)
                }
            })
        })
    }

    const onClickCard = () => {
        console.log('card clicked')

    }

    const renderEditView = () => {
        return matrix.map((row, rowIndex) => {
            return row.map((cell, colIndex) => {
                if (cell == 0) {

                    return (
                        <ThemeProvider theme={customTheme}>
                            <Grid item xs={2} key={colIndex}>
                                <div   key={`${rowIndex}-${colIndex}`}
                                    className={`card ${cell === 1 ? 'draggable' : ''}`}
                                    draggable={cell === 1}
                                    onDragStart={(e) => handleDragStart(e, rowIndex, colIndex)}
                                    onDrop={(e) => handleDrop(e, rowIndex, colIndex)}
                                    onDragOver={(e) => e.preventDefault()}>
                                <EditCard>{<AddIcon sx={{ fontSize: 40, color: 'white' }} />}</EditCard>
                                </div>
                            </Grid>
                        </ThemeProvider>
                    )

                }
                else {
                    return (
                        <ThemeProvider theme={customTheme}>
                            <Grid item xs={2} key={colIndex}>
                            <div   key={`${rowIndex}-${colIndex}`}
                                    className={`card ${cell === 1 ? 'draggable' : ''}`}
                                    draggable={cell === 1}
                                    onDragStart={(e) => handleDragStart(e, rowIndex, colIndex)}
                                    onDrop={(e) => handleDrop(e, rowIndex, colIndex)}
                                    onDragOver={(e) => e.preventDefault()}>
                                <EditCard border="true" onClick={() => onClickCard()}>xs=3</EditCard>
                                </div>
                            </Grid>
                        </ThemeProvider>
                    )
                }
            })
        })
    }

    const renderGrid = () => {
        if (mode.mode == 0) {
            { return renderView() }
        }
        else {
            { return renderEditView() };
        }
    }


    return (
        <Grid container spacing={2}>
            {
                renderGrid()
            }
        </Grid>
    );
}
