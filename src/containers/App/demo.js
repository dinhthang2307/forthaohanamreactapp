import React from 'react';
import { useState } from 'react';
import { DragDropContext, Draggable, Droppable } from 'react-beautiful-dnd';
import Grid from '@mui/material/Grid';
import { styled, createTheme, ThemeProvider } from '@mui/material/styles';
import Paper from '@mui/material/Paper';
import AddIcon from '@mui/icons-material/Add';

export default function Maingrid(mode) {
    // ... (same as your existing code)

    const onDragEnd = (result) => {
        // Reorder the matrix elements after drag-and-drop
        if (!result.destination) return;

        const updatedMatrix = [...matrix];
        const [removed] = updatedMatrix[result.source.droppableId].splice(result.source.index, 1);
        updatedMatrix[result.destination.droppableId].splice(result.destination.index, 0, removed);

        setMatrix(updatedMatrix);
    };

    const renderDraggableGrid = () => {
        return (
            <DragDropContext onDragEnd={onDragEnd}>
                <Droppable droppableId="matrix">
                    {(provided) => (
                        <Grid container spacing={2} {...provided.droppableProps} ref={provided.innerRef}>
                            {matrix.map((row, rowIndex) => (
                                <React.Fragment key={rowIndex}>
                                    {row.map((col, colIndex) => (
                                        <Draggable key={JSON.stringify({ row: rowIndex, col: colIndex })} draggableId={JSON.stringify({ row: rowIndex, col: colIndex })} index={colIndex}>
                                            {(provided) => (
                                                <Grid item xs={2} ref={provided.innerRef} {...provided.draggableProps} {...provided.dragHandleProps}>
                                                    {col === 0 ? (
                                                        <ViewCard></ViewCard>
                                                    ) : (
                                                        <EditCard border="true" onClick={() => onClickCard()}>
                                                            <AddIcon sx={{ fontSize: 40, color: 'white' }} />
                                                        </EditCard>
                                                    )}
                                                </Grid>
                                            )}
                                        </Draggable>
                                    ))}
                                </React.Fragment>
                            ))}
                        </Grid>
                    )}
                </Droppable>
            </DragDropContext>
        );
    };

    return (
        <ThemeProvider theme={customTheme}>
            {renderDraggableGrid()}
        </ThemeProvider>
    );
}
