/**
 *
 * App
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 */

import React from 'react';
import { useState } from 'react';
import './App.css';
import ButtonAppBar from '../../components/AppBar';
import TabList from '../../components/Tabs';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Maingrid from '../../components/Grid';

export default function App() {
  const [mode, useMode] = useState(0); // 0 is view, 1 is edit

  const buttonStyle = {
    color: 'white', // Set the text color to white
    display: mode === 0 ? 'none' : 'inline-block', // Hide the button when mode is 0
  };

  const editBtnFunction=()=>{
    useMode(1)
    const editBtn = document.getElementById("editBtn");
    editBtn.style.display="none"
    const cancleEditBtn = document.getElementById("CancleEditBtn");
    cancleEditBtn.style.display="inline"
  }

  const cancleEditBtnFunction = ()=>{
    useMode(0)
    const editBtn = document.getElementById("editBtn");
    editBtn.style.display="inline"
    const cancleEditBtn = document.getElementById("CancleEditBtn");
    cancleEditBtn.style.display="none"
  }
  return (
    <div className="App">
  
    <body className={mode==1 ? 'editing': null}>
      <ButtonAppBar/>
      <Box display="grid" gridTemplateColumns="repeat(12, 1fr)" gap={2}>
      
      <Box sx={{ display: 'flex', alignItems: 'center', gridColumn: 'span 8' }}>
          <TabList mode={mode}/>
          <Button style={buttonStyle}>move</Button>
      </Box>
      <Box gridColumn="span 4">
        <Button className="btn btn-primary" id="editBtn" onClick={()=> editBtnFunction()} >edit</Button>
        <Button className="btn btn-primary text-white" id="CancleEditBtn" onClick={()=> cancleEditBtnFunction()} style={{display: 'none', color:'white'}}>cancle edit</Button>

      </Box>
      <Box gridColumn = "span 12">
        <Maingrid mode={mode}/>
      </Box>
    </Box>
    </body>
  </div>
  );
}